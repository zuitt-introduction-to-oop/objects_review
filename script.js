// alert("Hello, B249!")

/* let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// What array method can we use to add an item at the end of the array?

students.push("Kristine");
console.log(students);

// What array method can we use to add an item at the start of the array?

students.unshift("Pinky");
console.log(students);

// What array method can we use to delete an item of the array?

students.pop("Pinky");
console.log(students);

// Nag-rremove ng first item 

students.shift("Kristine");
console.log(students);

What is the difference between splice() and slice()?
 splice - changes the original array; mutator methods; remove and adding items from a starting index
 slice - non-mutator methods; copies a portion from a starting index and returns a new array from it

 Iterator Methods - loop over the items of an array

 forEach()
 map()
 every()
*/

// Mini Activity:

let arrNum = [15, 20, 25, 30, 11, 7];

/*
Using forEach(), check if every item in the array is divisible by 5. If they are, log in the console "<num> is divisible by 5." If not, log in the console "false".

Kindly send the screenshot of the output in our Google chat.
*/

arrNum.forEach(num => {
  if (num % 5 === 0) {
    console.log(`${num} is divisible by 5`);
  } else {
    console.log(false);
  }
})

// Math Object - allows you to perform Math tasks on numbers

console.log(Math);
console.log(Math.E); //Euler's number
console.log(Math.PI); //PI
console.log(Math.SQRT2); //square root of 2
console.log(Math.SQRT1_2); //square root of 1/2
console.log(Math.LN2); //natural logarithm of 2
console.log(Math.LN10); //natural logarithm of 10
console.log(Math.LOG2E); // base 2 log e
console.log(Math.LOG10E); // base 10 log e

// Methods for rounding a number to an integer
console.log(Math.round(Math.PI));
console.log(Math.ceil(Math.PI));
console.log(Math.floor(Math.PI));
console.log(Math.trunc(Math.PI));

// Returns a square root of a number
console.log(Math.sqrt(3.14));

// Lowest value in a list of arguments
console.log(Math.min(-1, -2, -4, -100, 0, 1, 3, 5, 6));

// Exponent Operator
const number = 5 ** 2;
console.log(number);

console.log(Math.pow(5, 2));

/* Quiz

1. How do you create arrays in JS?
You use syntax:
const array_name = [item1, item2, ...];   

2. How do you access the first character of an array?
You use string indexing (str["0"]). For example, 

*/

const str = "Disney+";
const firstChar = str["0"]
console.log(firstChar); //D

/*
3. How do you access the last character of an array?
You use slice (str.slice(-1)). For example,
*/

const lastChar = str.slice(-1);
console.log(lastChar); //+

/*
4. What array method searches for, and returns the index of a given value in an array?
indexOf()

5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
forEach()

6. What array method creates a new array with elements obtained from a user-defined function?
map()

7. What array method checks if all its elements satisfy a given condition?
every()

8. What array method checks if at least one of its elements satisfies a
given condition?
some()

9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
False

10. True or False: array.slice() copies elements from original array and returns these as a new array.
True

Activity

1. 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
*/

let students = ["John", "Joe", "Jane", "Jessie"];

function addToEnd(students, additional_student) {
  if (typeof additional_student === "string") {
    students.push(additional_student);
    console.log(students);
  } else {
    console.log("error - can only add strings to an array");
  }
}

addToEnd(students, "Ryan");
addToEnd(students, 045);

/* 
2. Create a function named addToStart that will add a passed in string to
the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
*/

function addToStart(students, additional_student) {
  if (typeof additional_student === "string") {
students.unshift(additional_student);
    console.log(students);
  } else {
    console.log("error - can only add strings to an array");
  }
}

addToStart(students, "Tess");
addToStart(students, 033);

/*
3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
*/

function elementChecker(students, selected_student) {
  if (students.includes("Jane")){
    console.log(true);
  } else {
    console.log("error - passed in array is empty");
  }
}

elementChecker(students, "Jane");
elementChecker([], "Jane");

/*
4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:
● if array is empty, return "error - array must NOT be empty"
● if at least one array element is NOT a string, return "error - all array elements must be strings"
● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
● if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
● if every element in the array ends in the passed in character, return true. Otherwise return false.
Use the students array and the character "e" as arguments when testing.
*/

function checkAllStringsEnding(students, letter) {
  if (students.length === 0) {
    console.log("error - array must NOT be empty");
  } else if (typeof letter !== "string") {
    console.log("error - 2nd argument must be of data type string");
  } else if (letter > 1) {
    console.log("error - 2nd argument must be a single character");
  // } else if (students.every(function(currentValue, index, arr), "e")) {
   // console.log(true);
//  } else {
  //  console.log(false);
  }
}

checkAllStringsEnding(students, "e");
